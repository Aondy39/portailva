Installation de PortailVA sous Linux
====================================

Pour installer une version locale de PortailVA sur GNU/Linux, veuillez suivre les instructions suivantes.

- Si une commande ne passe pas, essayez de savoir pourquoi avant de continuer.
- Si une erreur s'est glissée dans la doc, ou si la doc est obsolète, ouvrez un ticket sur notre dépôt [gitlab](https://gitlab.com/sia-insa-lyon/portailva/issues/new).
- Si malgré tout vous ne parvenez pas à installer PortailVA, n'hésitez pas à nous [contacter](mailto:portailva@asso-insa-lyon.fr).

**Attention** : la procédure décrite dans ce guide sert à l'installation d'une instance de développement de PortailVA. Si vous souhaitez déployer une instance de production, nous vous préconisons l'utilisation de Docker.

Prérequis
=========

Certaines des commandes d'installation (débutant par ``apt-get``) sont données ici pour Ubuntu et ses dérivés, pour lesquels il est sûr qu'elles fonctionnent. Si vous utilisez une distribution différente, le nom des paquets à installer devrait être fort semblables, n'hésitez dès lors pas à employer la fonction "recherche" de votre gestionnaire de paquet préféré. Les autres commandes sont génériques et indépendantes de la distribution utilisée.

PortailVA a besoin des dépendances suivantes :

- git : ``apt-get install git``

L'utilisation du répertoire GitLab nécessite la mise en place d'une connexion SSH. Vous devez avoir [configuré](https://gitlab.com/help/ssh/README#locating-an-existing-ssh-key-pair) votre compte en conséquence pour pouvoir cloner le répertoire.

1ère solution - Script d'installation
=====================================

Clonez Portail VA et allez dans le dossier racine : ```git clone git@gitlab.com:sia-insa-lyon/portailva.git portailva && cd portailva```
Creez un dossier pour les fichiers et les ressources des utilisateurs : ```mkdir mediafiles && sudo chmod -R 777 mediafiles```

Le script d'installation [suivant](../bash/install_without_docker.sh) permet d'installer et de démarrer Portail VA directement sur une machine sous Ubuntu 19.10.

Il vous faudra exécuter ce dernier à la racine du répertoire de Portail VA en tant que super-utilisateur.

Introduction pour les solutions 2 et 3 - Installation de Docker 
===============================================================

Le projet a besoin de [Docker](https://www.docker.com/) pour mettre en place le SGBD nécessaire pour l'utilisation du back-end.
Pour cette partie, vous pouvez quitter votre espace de travail virtualenv.

Dans l'attente d'un Faker pour générer une base de test, il vous sera nécessaire de disposer d'un Dump de la base de donnée actuelle. Pour l'obtenir, contactez-nous.

Avant de procéder à l'installation, nous allons nettoyer les précédentes installations de Docker :
```bash
    sudo apt autoremove docker.io docker-ce docker-compose docker
 ```

Puis, nous allons installer Docker et ajouter l'utilisateur courant (<b>à définir</b>) au groupe Docker :
```bash
    sudo curl https://get.docker.com | bash
    sudo adduser your_system_user docker
 ```

Déconnectez-vous de votre session et reconnectez vous pour que l'ajout de groupe puisse prendre effet.

Vous pouvez vérifier que l'installation a réussi avec la commande ``docker info``.

2e solution - Docker-compose
=========================================================

Clonez Portail VA et allez dans le dossier racine : ```git clone git@gitlab.com:sia-insa-lyon/portailva.git portailva && cd portailva```

Creez un dossier pour les fichiers et les ressources des utilisateurs : ```mkdir mediafiles && sudo chmod -R 777 mediafiles```

Pour cette solution, vous aurez besoin de docker-compose. Pour cela, utilisez ```sudo apt install docker-compose```

Mettons en place le fichier d'environnement :
```bash
    echo "APP_DEBUG=True" >> .env
    # Pour pouvoir envoyer des mails, remplacez les X et les Y
    echo "MAILGUN_API_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" >> .env
    echo "MAILGUN_DOMAIN=YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY.mailgun.org" >> .env
 ```

Nous allons construire les conteneurs Docker et lancer Portail VA :
```bash
    docker-compose up -d                            # Vous pouvez retirer l'option d si vous voulez avoir accès aux logs directement dans le shell
    # Si vous voulez créer un compte super-utilisateur
    docker-composer exec app bash                   # Rentrez dans le bash du conteneur de Portail VA
    pipenv run python manage.py createsuperuser     # (Optionnel) Pour créer un super utilisateur (Attention : mettez bien le même pseudo que l'adresse mail du compte pour pouvoir vous connecter avec ce compte)
 ```

Portail VA devrait être accessible sur `http://localhost:8000`. Pour stopper Portail VA, utilisez ```docker-compose down```

3e solution - Installation manuelle et configuration de `virtualenv`
=========================================================

Nous vous conseillons de créer un utilisateur dédié à PortailVA (nommé ``portailva`` pour les besoins de cette documentation). Les sources seront téléchargées dans le répertoire de cet utilisateur.

```bash
    git clone git@gitlab.com:sia-insa-lyon/portailva.git portailva
    cd portailva
    mkdir mediafiles && sudo chmod -R 777 mediafiles    # Création d'un dossier pour les fichiers et les ressources des utilisateurs
    virtualenv portailva                                # Création de l'environnement Python virtuel
 ```

**À chaque fois** que vous souhaitez travailler dans votre environnement, activez-le via la commande suivante :

```bash
    source portailva/bin/activate # PAS sudo
 ```

Pour sortir de votre environnement, tapez ``deactivate``.

Une documentation plus complète de cet outil est disponible [ici](http://docs.python-guide.org/en/latest/dev/virtualenvs/).

3.1 Prérequis (suite pour la solution 3)
====================================

PortailVA a besoin des dépendances suivantes :

- python3 : ``apt-get install python3``
- python-dev : ``apt-get install python3-dev``
- pip : ``apt-get install python3-pip``
- libpq-dev : ``apt-get install libpq-dev``
- virtualenv : ``apt-get install virtualenv``
- curl : ``apt-get install curl``
- npm : ``apt-get install nodejs npm`` (<b>Attention :</b> Un moyen plus propre pour installer npm sera décrit plus tard.)

3.2 Installation des dépendances
============================

Le projet fait appel aux gestionnaires ``npm`` et ``pip`` pour récupérer un certain nombre de dépendances. Il est donc nécessaire d'installer Node.js :

```bash
    # Ajout des dépôts de Node.js au gestionnaire de packages, prenez la version la plus récente disponible (v12 au moment de la rédaction du guide)
    curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash
    apt-get install nodejs
    sudo npm install -g npm     # Mise à jour de npm si une version plus récente existe
 ```

Il nous faudra le gestionnaire ```pipenv```. Pour éviter de devoir manipuler la variable PATH de votre terminal, vous pourrez installer globalement ce dernier via la commande suivante :
```bash
    # Cette commande ne doit pas être utilisée en parallèle de virtualenv, si vous utilisez ce dernier, quittez-le avant de lancer cette commande !
    sudo -H pip3 install pipenv
 ```

Nous pouvons ensuite poursuivre avec l'installation des dépendances. Placez vous dans votre environnement de travail ```virtualenv``` si ce n'est pas déjà fait.
Exécutez les commandes suivantes (ignorez l'erreur associée à ```Django``` lors de l'exécution de ```pipenv```) :

```bash
    pip3 install Django==2.2            # Installation de Django pour Python 3 (pipenv ne gére que les dépendences python2)
    pipenv install                      # Installation des autres dépendences Python
    npm install --save-dev --save       # Installation pour le linter JavaScript
 ```

3.3 Lancer la base de donnée
========================

Maintenant que vous disposez de Docker, vous pouvez lancer l'instance contenant la base de donnée au sein de votre espace de travail ```virtualenv```.
Après l'importation des données, vous pourrez utiliser cette commande directement pour mettre en place la base lors de vos tests.

```bash
#   Exécutez l'instance pour mettre en route le SGBD (assurez-vous d'utiliser virtualenv pour lancer cette commande)
docker run --rm --name database -it -v ~/.db-postgre:/var/lib/postgresql/data -p 5432:5432 -e POSTGRES_USER=portailva -e POSTGRES_DB=portailva -e POSTGRES_PASSWORD=portailva postgres
 ```

(Optionnel) Une fois la base lancée, si la base est vide, il nous faut importer les données dans celle-ci depuis un autre terminal.

```bash
#   Importez le contenu du dump dans l'instance docker pour l'utilisateur portailva
docker exec -i database psql -U portailva portailva < postgres-portailva-daily-*************.psql
```

3.4 Configurer Django pour votre instance Docker
============================================

Pour pouvoir lancer proprement Django, créez un fichier ``.env`` à la racine du projet. Ajoutez, dans ce dernier, le contenu suivant (les deux premières sont obligatoires) :
```bash
DATABASE_URL=postgres://portailva:portailva@localhost/portailva
APP_DEBUG=True

BDE_PHONE=0404040404
HOST=localhost
SITE_DNS=localhost
MAILGUN_API_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
MAILGUN_DOMAIN=YYYYYYYYYYYYYYYYYYYYYYYYYYYY.mailgun.org
 ```

3.5 Lancer PortailVA
================

Placez vous dans votre environnement ```virtualenv``` à la racine du projet et lancez les trois commandes qui suivent.
```bash
    pipenv run python manage.py migrate             # Permet la création ou mise à jour du schéma de base de données
    pipenv run python manage.py collectstatic       # Permet la mise à jour des fichiers statiques
    pipenv run python manage.py createsuperuser     # (Optionnel) Pour créer un super utilisateur (Attention : mettez bien le même pseudo que l'adresse mail du compte pour pouvoir vous connecter avec ce compte)
    pipenv run python manage.py runserver 8000      # Lance le serveur de développement sur le port 8000
 ```

Si vous rencontrez un problème avec la commande ```python``` ou ```pipenv```, vérifiez que vous utilisez bien l'exécutable python de votre environnement (quelque chose comme ```~/.local/share/virtualenvs/portailva-XXXXXXX/bin/python```) et utilisez les commandes suivantes :
```bash
    python manage.py migrate
    python manage.py collectstatic
    python manage.py createsuperuser     # (Optionnel)
    python manage.py runserver 8000
 ```
