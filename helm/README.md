# PVA Helm Chart

This is a Helm Chart to deploy Portail VA on Kubernetes.
The Chart is quite minimal to answer our needs and there is not a lot of customisation options (you can't use exiting PVC or secrets, use imagePullSecrets...), this is on purpose but feel free to adapt it to your needs or just open a PR to allow more customisation.

## Prerequisites

Have a kubernetes Cluster with a dynamic volume provisioner for RWX volumes.
The Chart has only be tested on Kubernetes 1.18

## How to deploy

Currently we do not have a Helm Chart repository, we could use gitlab container registry but Helm is not yet fully supported (see [here](https://gitlab.com/gitlab-org/gitlab/-/issues/18997)).  
Hence you will need to clone this repo, `cd` into it, `checkout` to this branch and simply do `helm install my-release helm/`. The default should be enough for a minimal installation. But you should have a look at the values before installing, you have to use your own values if you want an ingress to access your deployment for example.

## Values

### PVA Values

| Value | Default | Comment |
|:------|---------|---------|
| `tags.postgresql` | true | set to `false`if you don't wand to deploy a postgresql database |
| `image.repository` | registry.gitlab.com/sia-insa-lyon/portailva:latest ||
| `tags.postgresql`| true | Wether to deploy a postgresql Helm Chart or not |
| `image.pullPolicy` | Always ||
| `autoscaling.enabled` | false ||
| `autoscaling.minReplicas` | 1 ||
| `autoscaling.maxReplicas` | 5 ||
| `autoscaling.targetCPUUtilizationPercentage` | 90 ||
| `resources.request.cpu` | 300m ||
| `resources.request.memory` | 500m ||
| `resources.limits.cpu` | 500m ||
| `resources.limits.memory` | 1Gi ||
| `ingress.enabled` | true ||
| `ingress.enabled` | portail.asso-insa-lyon.fr ||
| `service.type` | ClusterIP ||
| `service.port` | 80 ||
| `secret.mgApiKey` | nil
| `env.serverEmail` | noreply@bde-insa-lyon.fr ||
| `env.adminEmail` | bde.equipe.orgaif.dev@led.insa-lyon.fr ||
| `env.apiDns` | portail.asso-insa-lyon.fr ||
| `env.mailgunDomain` | mg.asso-insa-lyon.fr ||
| `env.appDebug` | `False` ||
| `env.djangoSettingsModule` | portailva.settings ||
| `env.defaultFromEmail` | no-reply@asso-insa-lyon.fr ||
| `env.siteDNS` | portail.asso-insa-lyon.fr ||
| `env.siteUrl` | <https://portail.asso-insa-lyon.fr> ||
| `env.wsgiApp` | portailva.wsgi ||
| `env.bdePhone` | 0472438229 ||
| `env.contact_fb` | https://www.facebook.com/VAINSALyon/ ||
| `env.contact_insta` | https://www.instagram.com/vainsalyon/ ||
| `env.contact_discord` | https://discord.gg/aQYkCDe ||
| `env.mention_role` | Président du BdE INSA Lyon ||
| `env.mention_mail` | sg@bde-insa-lyon.fr ||
| `storage.sizeMedia` | 1Gi ||
| `storage.sizeStatic` | 5Gi ||

### Database Values

| Value | Default | Comment |
|:------|---------|---------|
| `postgresql.postgresql.username` | postgres | use this value if you don't deploy a postgresql db to give django the db user |
| `postgresql.postgresql.password` | postgres | use this value if you don't deploy a postgresql db to give django the db password |
| `postgresql.postgresql.portailva` | portailva | use this value if you don't deploy a postgresql db to give django the db name |
| `postgresql.persitence.enabled` | true ||
| `postgresql.persitence.storageClass` | standard ||
| `postgresql.persitence.size` | 1Gi ||

### Backup Values

| Name | Default Value | Comments |
|:-----|---------------|----------|
| `image.repository` | gitlab-registry.in2p3.fr/cc-in2p3-devops/openshift-origin/openshift-images/backup_ceph_s3:latest ||
| `image.pullPolicy` | Always ||
| `cron` | "@daily" ||
| `compress` | true | If set to `true` will create a `tgz` archive, if you backup a directory, you have to set this value to true |
| `dataPath` | /data/ | The path to the file or directory you want to backup (depends on the mountPath you chose for the volume with the data) ||
| `bucketName` || The name of the bucket where you'll store the backup (no `/` at the endof the bucket name) |
| `fileName` || Name of the file in the S3 bucket, don't forget the `.tgz` of `compress` is `true` |
| `accessKey` |||
| `secretKey` |||
| `s3Url` |||
| `pvcName` || Name of the `persitentVolumeClaim` to access the data to backup, the backup container will mount it in read only mode ||
| `mountPath` | /data/ | Where to mount the volume in the backup container |
