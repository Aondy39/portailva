-i https://pypi.org/simple
appdirs==1.4.4
certifi==2020.12.5
chardet==4.0.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
cssselect==1.1.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
cssutils==2.2.0; python_version >= '3.6'
defusedxml==0.7.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
distlib==0.3.1
dj-database-url==0.5.0
django-anymail==6.0.1
django-bootstrap-form==3.4
django-bootstrap3-datetimepicker-2==2.8.2
django-ckeditor==5.7.1
django-crispy-forms==1.7.2
django-dotenv==1.4.2
django-js-asset==1.2.2
django-premailer==0.2.0
django==2.2.18
djangorestframework==3.12.4
et-xmlfile==1.0.1
filelock==3.0.12
gitdb==4.0.5
gitpython==3.1.7
icalendar==4.0.3
idna==2.10; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
jdcal==1.4.1
jinja2==2.11.3
lxml==4.6.3; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
markupsafe==1.1.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
oauthlib==3.1.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
openpyxl==2.6.2
pbr==5.5.1; python_version >= '2.6'
pillow==8.1.2
pipenv-to-requirements==0.7.1
pipenv==2020.11.15; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
premailer==3.0.0
psycopg2-binary==2.8.3
pyjwt==2.0.1; python_version >= '3.6'
python-dateutil==2.8.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
python-magic==0.4.15
python-social-auth==0.2.21
python3-openid==3.2.0
pytz==2021.1
requests-oauthlib==1.3.0
requests==2.25.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
six==1.15.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
smmap==3.0.5; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
sqlparse==0.4.1; python_version >= '3.5'
urllib3==1.26.4; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4' and python_version < '4'
uwsgi==2.0.18
virtualenv-clone==0.5.4; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
virtualenv==20.4.3; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
