from django.utils.deprecation import MiddlewareMixin


class SecurityHeadersMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        response["Strict-Transport-Security"] = "max-age=31536000; includeSubDomains; preload"
        response["Content-Security-Policy"] = "frame-ancestors 'none'"
        response["X-Frame-Options"] = "SAMEORIGIN"
        response["X-XSS-Protection"] = "1; mode=block"
        response["X-Content-Type-Options"] = "nosniff"
        response["Referrer-Policy"] = "no-referrer-when-downgrade"
        response["Permissions-Policy"] = "interest-cohort=()"
        return response
