import string

import unicodedata
from django.http import HttpResponse
from django.views.generic import TemplateView

from portailva.association.models import Association, Requirement
from portailva.export.mixins import AbleToExportMixin


class ExportView(AbleToExportMixin, TemplateView):
    template_name = 'export/export.html'

    def post(self, request):
        return self.export_xlsx(request.POST['filter'], request.POST.getlist('data'))

    @staticmethod
    def convertToTitle(num):
        title = ''
        alist = string.ascii_uppercase
        while num:
            mod = (num - 1) % 26
            num = int((num - mod) / 26)
            title += alist[mod]
        return title[::-1]

    def export_xlsx(self, category='ALL', datas=('BASIC', 'VALIDATIONS', 'PRESIDENT', 'DD', 'COVID', 'REGISTRATION','SUPPLEMENTARY', 'BANK')):

        # Create the Excel file
        import openpyxl
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=associations.xlsx'
        wb = openpyxl.Workbook()
        ws = wb.get_active_sheet()
        ws.title = "Associations"

        row_num = 0

        # Define columns to export
        columns = [ExportColumn('Nom', 'name', 30)]
        if 'BASIC' in datas:
            columns.append(ExportColumn('Acronyme', 'acronym'))
            columns.append(ExportColumn('Categorie', 'category.name', 22))
            columns.append(ExportColumn('ID', 'id'))
            if category == 'ALL':
                columns.append(ExportColumn('Active', 'is_active'))
            columns.append(ExportColumn('Validé', 'is_validated'))
        if 'VALIDATIONS' in datas:
            for requirement in Requirement.objects.get_all_active():
                columns.append(RequirementExportColumn(requirement))
        if 'PRESIDENT' in datas:
            columns.append(ExportColumn('Président (Nom)', 'mandates.last().peoples.first().__str__()'))
            columns.append(ExportColumn('Président (Téléphone)', 'mandates.last().peoples.first().phone'))
            columns.append(ExportColumn('Président (Email)', 'mandates.last().peoples.first().email'))
        if 'DD' in datas:
            columns.append(ExportColumn('Resp. DD (Nom)', 'mandates.last().peoples.filter(role__position=6)[0].__str__()'))
            columns.append(ExportColumn('Resp. DD (Email)', 'mandates.last().peoples.filter(role__position=6)[0].email'))
        if 'COVID' in datas:
            columns.append(ExportColumn('Resp. COVID (Nom)', 'mandates.last().peoples.filter(role__position=8)[0].__str__()'))
            columns.append(ExportColumn('Resp. COVID (Email)', 'mandates.last().peoples.filter(role__position=8)[0].email'))
        if 'REGISTRATION' in datas:
            columns.append(ExportColumn('SIREN', 'siren'))
            columns.append(ExportColumn('RNA', 'rna'))
        if 'BANK' in datas:
            columns.append(ExportColumn('IBAN', 'iban'))
            columns.append(ExportColumn('BIC', 'bic'))
        if 'SUPPLEMENTARY' in datas:
            columns.append(ExportColumn('Mail public', 'directory_entries.last().contact_address', 30))
            columns.append(ExportColumn('Local', 'directory_entries.last().place', 25))
            columns.append(ExportColumn('Date de création', 'created_at.strftime("%d/%m/%Y")', 15))
            columns.append(ExportColumn('Site web', 'directory_entries.last().website_url', 30))
            columns.append(ExportColumn('Facebook', 'directory_entries.last().facebook_url'))
        if 'BNP' in datas:
            columns = [ExportColumn('TIERS - NOM OU RAISON SOCIALE', 'name', options=['caps', 'accents']),
                       ExportColumn('TIERS - TYPE', value='Autre'),
                       ExportColumn('TIERS - REFERENCE INTERNE', 'id', prefix='CVA-'),
                       ExportColumn('TIERS - PAYS DE RESIDENCE', value='FRANCE'),
                       ExportColumn('TIERS - ADRESSE (NUMERO, VOIE)'),
                       ExportColumn('TIERS - COMPLEMENT ADRESSE'),
                       ExportColumn('TIERS - CODE POSTAL'),
                       ExportColumn('TIERS - VILLE'),
                       ExportColumn('TIERS - TELEPHONE'),
                       ExportColumn('TIERS - MOBILE'),
                       ExportColumn('TIERS - FAX'),
                       ExportColumn('TIERS - EMAIL'),
                       ExportColumn('TIERS - SIRET'),
                       ExportColumn('COMPTE - CODE SWIFT OU BIC', 'bic'),
                       ExportColumn('COMPTE - NUMERO IBAN', 'iban'),
                       ExportColumn('COMPTE - NUMERO BBAN'),
                       ExportColumn('COMPTE - NUMERO RIB'),
                       ExportColumn('COMPTE - CODE LOCAL (ABA OU ROUTING NUMBER)'),
                       ExportColumn('COMPTE - NOM DE LA BANQUE'),
                       ExportColumn('COMPTE - ADRESSE DE LA BANQUE'),
                       ExportColumn('COMPTE - COMPLEMENT ADRESSE DE LA BANQUE'),
                       ExportColumn('COMPTE - VILLE DE LA BANQUE'),
                       ExportColumn('COMPTE - PAYS DE LA BANQUE', value='FRANCE'),
                       ExportColumn('MANDAT- SDD MIGRE'),
                       ExportColumn('MANDAT- RUM'),
                       ExportColumn('MANDAT- SCHEMA'),
                       ExportColumn('MANDAT- TYPE'),
                       ExportColumn('MANDAT- DATE DE SIGNATURE')]

        # Create first line of document
        for col_num in range(len(columns)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            c.value = columns[col_num].name
            ws.column_dimensions[ExportView.convertToTitle(col_num+1)].width = columns[col_num].column_size

        # Fetch the set of data
        default_set = Association.objects.all().order_by('name')
        if category == 'ACTIVE':
            default_set = default_set.filter(is_active=True)
        elif category == 'DEAD':
            default_set = default_set.filter(is_active=False)

        # Write data for each element in the set
        for obj in default_set:
            row_num += 1
            for col_num in range(len(columns)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                c.value = columns[col_num].value_for(obj)

        wb.save(response)
        return response


class ExportColumn(object):

    def __init__(self, name, prop='', column_size=17, prefix='', value='', options=None):
        if options is None:
            options = []

        self.column_size = column_size
        self.name = name
        self.prop = prop
        self.prefix = prefix
        self.options = options
        self.value = value

    def value_for(self, association):
        value = str(self.prefix)
        try:
            if self.prop:
                val = str(eval("association."+self.prop))
            else:
                val = str(self.value)

            if 'accents' in self.options:
                val = ''.join((c for c in unicodedata.normalize('NFD', val) if unicodedata.category(c) != 'Mn'))
            if 'caps' in self.options:
                val = val.upper()

            return value + val

        except (AttributeError, IndexError): # Index error when nobody has the specified role
            return ''


class RequirementExportColumn(ExportColumn):
    def __init__(self, requirement, **kwargs):
        super().__init__(requirement.name, **kwargs)
        self.requirement = requirement

    def value_for(self, association):
        return self.requirement.is_achieved(association.id)





#########################################################################################################
# Bot'INSA Generator View - this class is left if someone wants to integrate again the generator to PVA
# You will need the following dependencies: impact = "==0.5.9" and fontawesome = "*"
#########################################################################################################
# This class get the infos on every active association and use the LaTeX template to generate a zip file
# containing a set of LaTeX file (to compile later on OverLeaf for example) and necessary asserts for them
#########################################################################################################
#
# class GeneratorBotINSAView(AbleToExportMixin, TemplateView):
#     template_name = 'export/botinsa.html'
#
#     def get_queryset(self):
#         return DirectoryEntry.objects.get_last_active()
#
#     def post(self, request):
#         categories = [
#             {"latex_filename": "animations_et_loisirs.tex",
#              "botinsa_category_names": {"Animation du campus", "Loisir"},
#              "latex_color": "red",
#              "botinsa_ids": {1, 6},
#              "placeholder_for_summary_list": "liste-assos-animations"},
#             {"latex_filename": "arts_et_spectacles.tex",
#              "botinsa_category_names": {"Arts et spectacles"},
#              "latex_color": "orange",
#              "botinsa_ids": {2},
#              "placeholder_for_summary_list": "liste-assos-arts"},
#             {"latex_filename": "cultures_internationales.tex",
#              "botinsa_category_names": {"Cultures internationales"},
#              "latex_color": "yellow",
#              "botinsa_ids": {3},
#              "placeholder_for_summary_list": "listes-assos-cultures"},
#             {"latex_filename": "departements_et_enseignements.tex",
#              "botinsa_category_names": {"Association de département"},
#              "latex_color": "green",
#              "botinsa_ids": {4},
#              "placeholder_for_summary_list": "liste-assos-departements"},
#             {"latex_filename": "humanitaire_et_social.tex",
#              "botinsa_category_names": {"Humanitaire et social"},
#              "latex_color": "blue",
#              "botinsa_ids": {5},
#              "placeholder_for_summary_list": "listes-assos-humanitaire"},
#             {"latex_filename": "sport.tex",
#              "botinsa_category_names": {"Sport"},
#              "latex_color": "purple",
#              "botinsa_ids": {7},
#              "placeholder_for_summary_list": "listes-assos-sport"},
#             {"latex_filename": "techniques_et_scientifique.tex",
#              "botinsa_category_names": {"Technique et scientifique"},
#              "latex_color": "brick",
#              "botinsa_ids": {8},
#              "placeholder_for_summary_list": "listes-assos-sciences"}
#         ]
#
#         template_intro = """\
#             \\documentclass{association}
#
#             \\justifying
#
#             \\begin{document}
#
#             """
#
#         template_asso = """\
#                 \\begin{{association}}
#                     [{association_acronym}]
#                     {{{association_name}}}
#                     [{association_mail}]
#                     [{association_website}]
#                     [{association_path_to_logo}]
#                     [{association_textual_schedule}]
#
#                     {association_description}
#                 \\end{{association}}
#
#             """
#
#         template_ending = """
#             \end{document}
#             """
#
#         category_color_text = """\
#                 \\SetCategoryColor{{{color}}}
#                 \\setlength{{\\parindent}}{{0.5cm}}
#                 \\setlength{{\\parskip}}{{0.1cm}}
#
#             """
#
#         logos_folder = os.path.join(settings.BASE_DIR, 'portailva/export/latex/logos_temp/')
#
#         original_main_template_file_path = os.path.join(settings.BASE_DIR, 'portailva/export/latex/template.tex')
#         main_template_file_path = os.path.join(settings.BASE_DIR, 'portailva/export/latex/main_output_dont_edit_directly.tex')
#
#         day_nb_to_name = {1: "Lundi", 2: "Mardi", 3: "Mercredi", 4: "Jeudi", 5: "Vendredi", 6: "Samedi", 7: "Dimanche"}
#
#         days_combinations = []
#         for L in range(8):
#             for subset in itertools.combinations([1, 2, 3, 4, 5, 6, 7], L):
#                 days_combinations.append(subset)
#         days_combinations = days_combinations[::-1]
#
#         def slugify(value, allow_unicode=False):
#             """
#             Convert to ASCII if 'allow_unicode' is False. Convert spaces to hyphens.
#             Remove characters that aren't alphanumerics, underscores, or hyphens.
#             Convert to lowercase. Also strip leading and trailing whitespace.
#             """
#             value = str(value)
#             if allow_unicode:
#                 value = unicodedata.normalize('NFKC', value)
#             else:
#                 value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
#             value = re.sub(r'[^\w\s-]', '', value).strip().lower()
#             return re.sub(r'[-\s]+', '-', value)
#
#         def is_valid_url(string):
#             regex = re.compile(
#                 r'^(?:http|ftp)s?://'  # http:// or https://
#                 r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
#                 r'localhost|'  # localhost...
#                 r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
#                 r'(?::\d+)?'  # optional port
#                 r'(?:/?|[/?]\S+)$', re.IGNORECASE)
#
#             return re.match(regex, string) is not None
#
#         def is_valid_mail(string):
#             regex = re.compile(r"[^@]+@[^@]+\.[^@]+")
#             return re.match(regex, string) is not None
#
#         def get_logo_path(asso_name, logo_url):
#             expected_file_name_base = slugify(asso_name)
#
#             # Make sure the folder exists
#             if not os.path.exists(logos_folder):
#                 os.makedirs(logos_folder)
#
#             # If file has already been downloaded, reuse it
#             for filename in os.listdir(logos_folder):
#                 file_path = os.path.join(logos_folder, filename)
#                 is_in_logos_folder = os.path.isfile(file_path) and expected_file_name_base == \
#                                      os.path.splitext(filename)[0]
#                 if is_in_logos_folder:
#                     return file_path
#
#             # Else download it and return the path to it
#             response = requests.get(logo_url, stream=True)
#             if response.status_code == 200:
#                 extension = mimetypes.guess_extension(response.headers["Content-Type"])
#                 extension = ".jpeg" if extension == ".jpe" else extension  # BUGFIX for JPEG images that don't get the right extension
#                 file_path = os.path.join(logos_folder, expected_file_name_base + extension)
#                 with open(file_path, 'wb') as f:
#                     response.raw.decode_content = True
#                     shutil.copyfileobj(response.raw, f)
#                 return file_path
#
#         def format_time(input_time):
#             """
#             Shortens the time string and replaces semicolons by "h" symbol, as is standard in France
#             :param input_time: time string in two semicolons format
#             :param is_start: is False, means that this is the ending time
#             :return: time string shortened as much as possible
#             """
#             # Always remove seconds
#             shortened_time = str(input_time)[:-3]
#
#             # Change ":" to "h"
#             shortened_time = shortened_time.replace(":", "h")
#
#             # Remove minutes if none
#             if shortened_time[-1] == "0" and shortened_time[-2] == "0":
#                 shortened_time = shortened_time[:-2]
#
#             return shortened_time
#
#         def get_missing_days(actual_days):
#             missing_days = []
#             for day in day_nb_to_name.keys():
#                 if day not in actual_days:
#                     missing_days.append(day)
#             return missing_days
#
#         def is_valid_day_combination(days_combination, days_for_time_interval, time_intervals_for_day):
#             # Check if for this combination, all time intervals are the same
#             for day in days_combination:
#                 if day in time_intervals_for_day:
#                     for time_interval in time_intervals_for_day[day]:
#                         if not set(days_combination).issubset(set(days_for_time_interval[time_interval])):
#                             return False
#                 else:
#                     return False
#             if len(days_combination) > 0:
#                 return True
#             else:
#                 return False
#
#         def is_to_textualize_day_combination(days_combination, days_to_textualize):
#             for day in days_combination:
#                 if day not in days_to_textualize:
#                     return False
#             return True
#
#         def time_interval_to_text(time_interval):
#             return "{starts_at} - {ends_at}".format(starts_at=format_time(time_interval[0]),
#                                                     ends_at=format_time(time_interval[1]))
#
#         def schedule_to_text(schedule_array):
#             days_for_time_interval = dict()  # {time_tuple: [day_id, day_id, ...]}
#             time_intervals_for_day = dict()
#
#             # Properly associate days with time intervals and time intervals with days
#             for time_interval in schedule_array.all():
#                 time_tuple = (time_interval.begins_at, time_interval.ends_at)
#                 if time_tuple in days_for_time_interval:
#                     days_for_time_interval[time_tuple].append(time_interval.day)
#                 else:
#                     days_for_time_interval[time_tuple] = [time_interval.day]
#
#                 if time_interval.day in time_intervals_for_day:
#                     time_intervals_for_day[time_interval.day].append(time_tuple)
#                 else:
#                     time_intervals_for_day[time_interval.day] = [time_tuple]
#
#             # Find all days combinations that have exactly the same schedules
#             valid_days_combinations = []
#             for days_combination in days_combinations:
#                 if is_valid_day_combination(days_combination, days_for_time_interval, time_intervals_for_day):
#                     valid_days_combinations.append(days_combination)
#             valid_days_combinations.sort(key=lambda item: item[0])
#
#             days_to_textualize = list(time_intervals_for_day.keys())
#             strings_to_join_for_schedule = []
#
#             for days_combination in valid_days_combinations:
#                 if is_to_textualize_day_combination(days_combination, days_to_textualize):
#                     common_time_intervals = time_intervals_for_day[days_combination[0]]
#                     common_time_intervals_as_text = map(time_interval_to_text, common_time_intervals)
#                     textual_interval = "; ".join(common_time_intervals_as_text)
#
#                     days_combination_text = ""
#
#                     # Open every day at this time interval
#                     if len(days_combination) == 7:
#                         days_combination_text = textual_interval + " tous les jours"
#                     elif 4 <= len(days_combination) <= 6:
#                         days_string = ""
#                         missing_days = get_missing_days(days_combination)
#                         if len(days_combination) == 6:
#                             days_string = day_nb_to_name[missing_days[0]]
#                         elif len(days_combination) == 5:
#                             days_string = "{first_day} et {second_day}".format(
#                                 first_day=day_nb_to_name[missing_days[0]],
#                                 second_day=day_nb_to_name[missing_days[1]])
#                         elif len(days_combination) == 4:
#                             days_string = "{first_day}; {second_day} et {third_day}".format(
#                                 first_day=day_nb_to_name[missing_days[0]],
#                                 second_day=day_nb_to_name[missing_days[1]],
#                                 third_day=day_nb_to_name[missing_days[2]])
#                         days_combination_text = textual_interval + " tous les jours sauf {days}".format(
#                             days=days_string)
#                     elif 1 <= len(days_combination) <= 3:
#                         days_string = ""
#                         if len(days_combination) == 3:
#                             days_string = "{first_day}; {second_day} et {third_day}".format(
#                                 first_day=day_nb_to_name[days_combination[0]],
#                                 second_day=day_nb_to_name[days_combination[1]],
#                                 third_day=day_nb_to_name[days_combination[2]])
#                         elif len(days_combination) == 2:
#                             days_string = "{first_day} et {second_day}".format(
#                                 first_day=day_nb_to_name[days_combination[0]],
#                                 second_day=day_nb_to_name[days_combination[1]])
#                         elif len(days_combination) == 1:
#                             days_string = day_nb_to_name[days_combination[0]]
#                         days_combination_text = textual_interval + " le {days}".format(days=days_string)
#
#                     strings_to_join_for_schedule.append(days_combination_text)
#                     for day in days_combination:
#                         days_to_textualize.remove(day)
#             schedule_text = ", ".join(strings_to_join_for_schedule)
#
#             return schedule_text
#
#         def check_asso_json_validity(asso_data):
#             checks = {"logo_url": (is_valid_url(asso_data.association.logo_url)),
#                       "contact_address": (asso_data.contact_address and is_valid_mail(asso_data.contact_address)),
#                       "description": (asso_data.description and 500 < len(asso_data.description) <= 900),
#                       "opening_hours": asso_data.opening_hours,
#                       }
#
#             report = ""
#             failed_checks_nb = 0
#             for criterion, check in checks.items():
#                 if not check:
#                     failed_checks_nb += 1
#                     if failed_checks_nb == 1:
#                         report += "The association '{name}' could not be added because of missing data:\n".format(
#                             name=asso_data.association.name)
#                     report += " - {criterion}\n".format(criterion=criterion)
#             if failed_checks_nb > 0:
#                 print(report)
#                 return False
#             else:
#                 return True
#
#         def add_directory_to_zip(zip_file, dir):
#             for root, dirs, files in os.walk(os.path.join(settings.BASE_DIR, 'portailva/export/latex/' + dir)):
#                 for file in files:
#                     zip_file.write(os.path.join(root, file), dir + '/' + file)
#
#         def tex_escape(text):
#             """
#                 :param text: a plain text message
#                 :return: the message escaped to appear correctly in LaTeX
#             """
#             conv = {
#                 '&': r'\&',
#                 '%': r'\%',
#                 '$': r'\$',
#                 '#': r'\#',
#                 '_': r'\_',
#                 '{': r'\{',
#                 '}': r'\}',
#                 '~': r'\textasciitilde{}',
#                 '^': r'\^{}',
#                 '\\': r'\textbackslash{}',
#                 '<': r'\textless{}',
#                 '>': r'\textgreater{}',
#             }
#             regex = re.compile(
#                 '|'.join(re.escape(str(key)) for key in sorted(conv.keys(), key=lambda item: - len(item))))
#             return regex.sub(lambda match: conv[match.group()], text)
#
#         response = HttpResponse(content_type='application/zip')
#         zip_file = zipfile.ZipFile(response, 'w')
#
#         assos_data = self.get_queryset()
#         # Reset main template tex file from base file
#         shutil.copyfile(original_main_template_file_path, main_template_file_path)
#
#         website_urls_to_check = ["--- Websites URL to check if work:"]
#
#         acronyms_to_check = ["--- Associations acronyms to check if actually meaningful:"]
#
#         schedules_to_check = ["--- Associations schedules to check by calling the associations resps:"]
#
#         assos_names_for_placeholder = dict()
#         with io.StringIO() as buf, redirect_stdout(buf):
#             for category in categories:
#                 with open(os.path.join(settings.BASE_DIR, 'portailva/export/latex/' + category["latex_filename"]), 'w') as latex_file:
#                     content = template_intro + category_color_text.format(color=category["latex_color"])
#
#                     print("Completed content generation for associations :")
#                     for asso_data in assos_data:
#                         if (asso_data.association.is_validated and asso_data.association.is_active
#                                 and asso_data.association.category.id in category["botinsa_ids"]):
#                             if check_asso_json_validity(asso_data):
#                                 asso_path_to_logo = get_logo_path(asso_data.association.name, asso_data.association.logo_url if asso_data.association.logo_url is not None else "")
#                                 asso_textual_schedule = schedule_to_text(asso_data.opening_hours)
#
#                                 if asso_data.website_url is None or asso_data.website_url == "":
#                                     if asso_data.facebook_url is not None and asso_data.facebook_url != "":
#                                         website_url = asso_data.facebook_url
#                                     elif asso_data.twitter_url is not None and asso_data.twitter_url != "":
#                                         website_url = asso_data.twitter_url
#                                     else:
#                                         website_url = ""
#                                 else:
#                                     website_url = asso_data.website_url
#
#                                 asso_description = tex_escape(asso_data.description)
#                                 asso_description = asso_description \
#                                     .replace("\u00A0", u" ") \
#                                     .replace(" !", u"\u00A0!") \
#                                     .replace(" ?", u"\u00A0?") \
#                                     .replace(" :", u"\u00A0:")
#                                 asso_description = asso_description.replace('\n', '\n        ')
#
#                                 asso_content = template_asso.format(association_acronym=tex_escape(
#                                     asso_data.association.acronym if asso_data.association.acronym is not None else ""),
#                                                                     association_name=tex_escape(asso_data.association.name),
#                                                                     association_mail=asso_data.contact_address,
#                                                                     association_website=tex_escape(
#                                                                         website_url.replace("facebook.com",
#                                                                                             "fb.me").replace("www.", "")),
#                                                                     association_path_to_logo=asso_path_to_logo,
#                                                                     association_textual_schedule=asso_textual_schedule if asso_textual_schedule != "" else "Pas de permanences.",
#                                                                     association_description=asso_description)
#                                 content += asso_content
#
#                                 print("- {asso_name}".format(asso_name=asso_data.association.name))
#                                 placeholder = category["placeholder_for_summary_list"]
#                                 if placeholder in assos_names_for_placeholder:
#                                     assos_names_for_placeholder[placeholder].append(tex_escape(asso_data.association.name))
#                                 else:
#                                     assos_names_for_placeholder[placeholder] = [tex_escape(asso_data.association.name)]
#
#                                 # Manual checks to do later
#                                 website_urls_to_check.append("{asso_name} : {website_url}".format(
#                                     asso_name=asso_data.association.name,
#                                     website_url=website_url
#                                 ))
#                                 if asso_data.association.acronym is not None:
#                                     acronyms_to_check.append("{asso_name} : {acronym}".format(
#                                         asso_name=asso_data.association.name,
#                                         acronym=asso_data.association.acronym
#                                     ))
#                                 if not asso_data.opening_hours:
#                                     schedules_to_check.append("{asso_name}".format(
#                                         asso_name=asso_data.association.name
#                                     ))
#
#                     content += template_ending
#                     latex_file.write(content)
#                     latex_file.close()
#
#                     with fileinput.FileInput(main_template_file_path, inplace=True, backup='.bak') as file:
#                         placeholder = category["placeholder_for_summary_list"]
#                         replacement_text = ", ".join(assos_names_for_placeholder[placeholder])
#                         for line in file:
#                             print(line.replace(placeholder, replacement_text), end='')
#
#                     zip_file.write(os.path.join(settings.BASE_DIR, 'portailva/export/latex/' + category["latex_filename"]), category["latex_filename"])
#
#             # Close redirection of STDOUT and dump it inside a new file in the zip
#             zip_file.writestr("STDOUT.txt",buf.getvalue())
#
#         # TODO remove created file before returning response
#         # TODO clean and refactor
#         add_directory_to_zip(zip_file,'backgrounds')
#         add_directory_to_zip(zip_file,'examples')
#         add_directory_to_zip(zip_file,'logos')
#         add_directory_to_zip(zip_file,'logos_temp')
#         zip_file.write(os.path.join(settings.BASE_DIR, 'portailva/export/latex/category.sty'), 'category.sty')
#         zip_file.write(os.path.join(settings.BASE_DIR, 'portailva/export/latex/botinsa-colors.tex'), 'botinsa-colors.tex')
#         zip_file.write(os.path.join(settings.BASE_DIR, 'portailva/export/latex/association.cls'), 'association.cls')
#         zip_file.write(os.path.join(settings.BASE_DIR, 'portailva/export/latex/template.tex'), 'template.tex')
#         zip_file.write(main_template_file_path, 'main_output_dont_edit_directly.tex')
#
#         # See RFC 5987, this allow the handling of problematic file name for some browser like safari
#         file_name_utf = 'BotINSA_V{}.zip'.format(datetime.now().strftime("%Y%m%d_%H%M%S"))
#         try:
#             file_name_ascii = file_name_utf.encode('ascii')
#             response['Content-Disposition'] = 'attachment; filename="{}"'.format(file_name_ascii) + \
#                                               "; filename*=UTF-8''{}".format(file_name_utf)
#         except UnicodeEncodeError:
#             response['Content-Disposition'] = "attachment; filename*=UTF-8''{}".format(file_name_utf)
#
#         return response

