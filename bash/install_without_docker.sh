#!/usr/bin/env bash

# Created by Gabriel Augendre - Modified by Matthieu Halunka - Run on Ubuntu 20.04 (22/06/2020)
# Install system dependencies
apt update && apt install -y git curl build-essential libreadline-dev zlib1g-dev libssl-dev libbz2-dev libsqlite3-dev libffi-dev libmagic-dev

# Install pyenv
curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
export PATH="/root/.pyenv/bin:$PATH"  # I needed this in my container to find `pyenv`'s binary. You can find the binary with `find / -name "*pyenv"`.
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Install the python version we want
pyenv install 3.8.3

# Clone PVA (uncomment if you already have it clonned)
# git clone https://gitlab.com/sia-insa-lyon/portailva.git
# cd portailva/

# Setup
echo "APP_DEBUG=True" >> .env
pyenv local 3.8.3

# Install pipenv
pip install pipenv

# Install python dependencies from `Pipfile`
pipenv install

# Migrate the DB (`./db.sqlite3` by default)
pipenv run python manage.py migrate

# Collect staticfiles
pipenv run python manage.py collectstatic --noinput

# Create a superuser
pipenv run python manage.py createsuperuser

# Run PVA on localhost
pipenv run ./manage.py runserver localhost:8000
